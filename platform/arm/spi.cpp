#include "spi.h"
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>

#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <linux/types.h>
#include <linux/spi/spidev.h>

SPI::SPI() : speed(0),delay(0)
{

}
void SPI::setup(Configuration &conf){
    Json::Value base=conf.root["spi"];

    name=base["device"].asString();
    speed=base["speed"].asInt();
    delay=base["delay"].asInt();

    memset(datain,0,sizeof(datain));
    memset(dataout,0,sizeof(dataout));

    fd = open(name.c_str(), O_RDWR);
    if (fd < 0) {
        perror("Radio open");

    }else{
        setSpeed(speed);
        dumpstat(name.c_str());
    }
}

void SPI::dumpstat(const char *name)
{
    __u8	lsb, bits;
    __u32	mode, speed;
    mode=SPI_MODE_0;
    if (ioctl(fd, SPI_IOC_WR_MODE, &mode) < 0) {
        perror("SPI wr_mode");
        return;
    }
    if (ioctl(fd, SPI_IOC_RD_MODE, &mode) < 0) {
        perror("SPI rd_mode");
        return;
    }
    if (ioctl(fd, SPI_IOC_RD_LSB_FIRST, &lsb) < 0) {
        perror("SPI rd_lsb_fist");
        return;
    }
    if (ioctl(fd, SPI_IOC_RD_BITS_PER_WORD, &bits) < 0) {
        perror("SPI bits_per_word");
        return;
    }
    if (ioctl(fd, SPI_IOC_RD_MAX_SPEED_HZ, &speed) < 0) {
        perror("SPI max_speed_hz");
        return;
    }

    printf("%s: spi mode 0x%x, %d bits %sper word, %d Hz max\n",
           name, mode, bits, lsb ? "(lsb first) " : "", speed);
}
void SPI::setSpeed(uint32_t &hz){
    int ret =0;
    ret = ioctl(fd, SPI_IOC_WR_MAX_SPEED_HZ, &hz);
    if (ret == -1)
        printf("can't set max speed hz\n");
    ret = ioctl(fd, SPI_IOC_RD_MAX_SPEED_HZ, &hz);
    if (ret == -1)
        printf("can't set max speed hz\n");

}

int SPI::transfer(int len)
{
    struct spi_ioc_transfer xfer[2];
    int status;
    memset(xfer, 0, sizeof xfer);

    xfer[0].tx_buf = (unsigned long)dataout;
    xfer[0].rx_buf = (unsigned long)datain;
    xfer[0].len = len;
    xfer[0].delay_usecs = delay;//200;
    status = ioctl(fd, SPI_IOC_MESSAGE(1), xfer);
    if (status < 0) {
        perror("SPI_IOC_MESSAGE");
        return -1;
    }
    return 0;
}
