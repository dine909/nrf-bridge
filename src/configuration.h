#ifndef CONFIGURATION_H
#define CONFIGURATION_H
#include "common.h"
#include "json.h"

class Configuration
{
public:
    Json::Value root;
    vector<string> irqPins;
    vector<string> cePins;
    Configuration();
    void configureGPIO();
};

#endif // CONFIGURATION_H
