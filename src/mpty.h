#ifndef MPTY_H
#define MPTY_H

#include "common.h"

class MPTY
{
    int rc;
public:
    int slave,fdm;
    MPTY();
    const char* ptsName(){
       return ::ptsname(fdm);
    }

     MPTY(const MPTY&){

     }

    int setup();
    ssize_t read(void * dest,size_t size);
    ssize_t write(void * dest,size_t size);
    void close();
    ~MPTY();
    const char* ptsname(){
        return ::ptsname(fdm);
    }

    virtual void destroy();
    void setRawMode(int f);
};

#endif // MPTY_H
