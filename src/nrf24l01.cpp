#include "nrf24l01.h"
#include "radio.h"
#include <mutex>
#include <condition_variable>
#include <poll.h>

#include <errno.h>
#include <fcntl.h>
#include <poll.h>
#include <stdio.h>
#include <algorithm>

SPI NRF24L01::spi;
ofstream *NRF24L01::CEfs;
char NRF24L01::REG_STATUS;
char NRF24L01::REG_CONFIG;
mutex M_spi;

#define VAL(x,y) (y<<x)
#define BIT(x) (1<<x)
#ifdef ARM
#include <iosfwd>
#define SET_CE(x) (*CEfs) << x ; CEfs->flush();
#else
#define SET_CE(x)
#endif

#define SET_RX(x) writeReg({CONFIG, BIT(PWR_UP) | BIT(EN_CRC) | (x*BIT(PRIM_RX)) });

NRF24L01::NRF24L01()
{

}

void NRF24L01::setupCE(Configuration &conf, int id = 0)
{
#ifdef ARM
    if(conf.cePins[id].size()>0)
        CEfs=new ofstream(conf.cePins[id]+"/value", ios::binary | ios::app);
#endif
}

void NRF24L01::writeAddrReg(string radioAddr, char regAddr)
{
    string addr;
    addr.push_back(regAddr);
    std::reverse(radioAddr.begin(),radioAddr.end());
    addr.append(radioAddr);
    Utils::dumpHex((void*)addr.c_str(),addr.size());
    writeReg(addr);
}
void runCmd(string cmd)
{
    system(cmd.c_str());
}

void NRF24L01::setup(Configuration &conf){

    setupCE(conf);
    spi.setup(conf);
    Radio::setup(conf);
    {
        lock_guard<mutex> guard(M_spi);
        char aw=(char)conf.root["radio"]["aw"].asUInt();
        SET_CE(0);
        sleep(1);
        char pcfg=
                (pipes.size()>5?BIT(5):0)  |
                (pipes.size()>4?BIT(4):0)  |
                (pipes.size()>3?BIT(3):0)  |
                (pipes.size()>2?BIT(2):0)  |
                (pipes.size()>1?BIT(1):0)  |
                (pipes.size()>0?BIT(0):0);
        writeReg({EN_AA,pcfg});
        writeReg({EN_RXADDR,pcfg});
        writeReg({SETUP_AW,(char)(aw-0x2)});
        writeReg({SETUP_RETR,(char)0x28});
        writeReg({RF_CH,(char)conf.root["radio"]["channel"].asUInt()});
        writeReg({RF_SETUP,(BIT(RF_DR) *0)| VAL(3,RF_PWR)});
        writeReg({STATUS,BIT(MAX_RT) | BIT(TX_DS) | BIT(RX_DR)});
        //        writeReg({OBSERVE_TX,0});


        for(uint pi=0;pi<pipes.size();pi++){
            RadioPipe &rp=pipes.at(pi);
            writeAddrReg(conf.root["radio"]["pipes"][pi]["addr"].asString(), (char)RX_ADDR_P0+pi);
            writeReg({(char)(RX_PW_P0+pi),0x20});
            threads.push_back(new thread(runCmd,conf.root["radio"]["pipes"][pi]["up"].asString()+rp.ptsname()));
        }
        writeAddrReg(conf.root["radio"]["txaddr"].asString(), (char)TX_ADDR);

        transfer({FLUSH_TX});
        transfer({FLUSH_RX});

        writeReg({FEATURE,BIT(EN_DPL)});
        writeReg({DYNPD,pcfg});



        SET_RX(1);
        SET_CE(1);
    }

    getStatus();
    //    transfer({(char)W_TX_PAYLOAD,1,2,3},4);

    irq(conf);

}

void NRF24L01::update(){
    memcpy(spi.dataout,"HELLO",6);
    spi.transfer(6);
}

string NRF24L01::transfer(string data, int len)
{

    string result=spi.transfer(data,len);
    REG_STATUS=spi.datain[0];
    return result;
}

void NRF24L01::writeReg(string data)
{
    data[0]|=W_REGISTER;
    NRF24L01::transfer(data);
}


char NRF24L01::readReg(string data)
{
    return NRF24L01::transfer(data,1+1)[1];
}
string NRF24L01::readReg(string data,int registers)
{
    string ret=NRF24L01::transfer(data,1+registers);
    ret.erase(ret.begin());
    return ret;
}

int get_lead(int fd) {
    int value;
    lseek(fd, 0, 0);

    char buffer[1024];
    int size = read(fd, buffer, sizeof(buffer));
    if (size != -1) {
        buffer[size] = 0;
        value = atoi(buffer);
    }
    else {
        value = -1;
    }

    return value;
}
void NRF24L01::getStatus()
{
    lock_guard<mutex> guard_SPI(M_spi);
    REG_CONFIG=readReg({CONFIG});
    {
#ifdef SHOWDEBUG
        printf("REG_STATUS: 0x%02x\t",REG_STATUS);
        printf("REG_CONFIG: 0x%02x\n",REG_CONFIG);
#endif
    }
}

void NRF24L01::irq(Configuration &conf){
    //    Configuration *conf=(Configuration*)conf;
    int pincount =conf.irqPins.size();
    int pipecount =pipes.size();
    int pollc=0;
    int fd[pincount];
    struct pollfd pfd_irq[pincount+pipecount];
    memset(&pfd_irq,0,sizeof(struct pollfd)*(pincount+pipecount));
#define POLL_ALL (pincount+pipecount)
#define POLL_RF (pincount)

    for(uint i=0;i<pincount;i++)
    {
        string pin=conf.irqPins[0]+"/value";
        fd[i]=open(pin.c_str(),O_RDONLY);
        pfd_irq[i].fd = fd[i];
        pfd_irq[i].events = POLLPRI;
        pfd_irq[i].revents = 0;
    }

    for(uint i=pincount;i<(pincount+pipecount);i++)
    {
        RadioPipe &rp=((RadioPipe&)pipes.at(i-pincount));
#ifdef SHOWDEBUG
        cout << "polling pipe: "<< (i-pincount) <<" fd: " << rp.fdm << endl;
#endif
        pfd_irq[i].fd = rp.fdm;
        pfd_irq[i].events = POLLIN;
        pfd_irq[i].revents = 0;
    }



    pollc=POLL_ALL;
    RadioPipe *sendingon=0;
    int delay=0;
    while(1){
        int wt=-1;
        //int ready = ;
#ifdef SHOWDEBUG
        cout << "polling " << pollc << endl;
#endif
        int curpollv=pollc;
        int ready=poll(pfd_irq, curpollv,wt);
        //         cout<< "ready: " << ready << endl;

        //        getStatus();
        for(uint i=0;i<pincount;i++)
        {
            if (pfd_irq[i].revents != 0 )
            {
                if( get_lead(fd[i])==0) {
                    getStatus();

                    lock_guard<mutex> guard_SPI(M_spi);
#ifdef SHOWDEBUG
                    cout<<"IRQ: ";
#endif
                    if(REG_STATUS & BIT(RX_DR))
                    {
                        do
                        {
#ifdef SHOWDEBUG
                            cout<<"RX_DR " ;
#endif
                            char datapipe=(REG_STATUS>>RX_P_NO)&0x7;
                            if(datapipe<pipecount)
                            {
                                char datalength=readReg({(char)R_RX_PL_WID});
                                string rxdata=readReg({(char)R_RX_PAYLOAD},datalength);
                                writeReg({STATUS,BIT(RX_DR)});
                                //                    rxdata.at(datalength)=0;
#ifdef SHOWDEBUG
                                cout<< "PIPE:"<< ((int)datapipe)<<"\tLEN: " <<((int)datalength) << endl;
                                //                            cout << "DATA: " << rxdata << endl;
#endif
                                {
                                    RadioPipe &rp=((RadioPipe&)pipes.at(datapipe));
                                    rp.write((void*)rxdata.c_str(),datalength) ;
                                }

#ifdef SHOWDEBUG
                                Utils::dumpHex(rxdata.c_str(),datalength);
#endif
                            }
                        }while(!(readReg({(char)FIFO_STATUS}) & BIT(RX_EMPTY)));

                    }
                    if(REG_STATUS & BIT(MAX_RT))
                    {
#ifdef SHOWDEBUG
                        cout<<"MAX_RT " ;
#endif
                        transfer({FLUSH_TX});
                        writeReg({STATUS,BIT(MAX_RT)});
                        sendingon=0;
                        pollc=POLL_ALL;
                        SET_RX(1);
                        delay+=10;
                        usleep(delay*50);
                        wt=200;
                     }
                    if(REG_STATUS & BIT(TX_DS))
                    {
#ifdef SHOWDEBUG
                        cout<<"TX_DS ";
#endif
                        writeReg({STATUS,BIT(TX_DS)});

                        sendingon->messagetosendlen=0;
                        sendingon=0;
                        pollc=POLL_ALL;
                        SET_RX(1);
                        usleep((delay<<1)*50);
                        wt=-1;
                    }

                    if(REG_STATUS & TX_FULL)
                    {
#ifdef SHOWDEBUG
                        cout<<"TX_FULL ";
#endif
                        writeReg({STATUS,BIT(TX_FULL)});
                    }
#ifdef SHOWDEBUG
                    cout << endl;
#endif
                }
            }
        }

        for(uint i=pincount;i<curpollv;i++)
        {
            RadioPipe &mp=((RadioPipe&)pipes[i-pincount]);
            if(mp.messagetosendlen==0)
            {
                if (pfd_irq[i].revents & POLLIN )
                {
                    char msg[33];
                    mp.messagetosendlen = mp.read(msg, sizeof(msg) - 1);
                    string output;
                    output.append({(char)W_TX_PAYLOAD});
                    output.append(msg,mp.messagetosendlen);
                    mp.messagetosend=output;
                }else if(pfd_irq[i].revents != 0){
#ifdef SHOWDEBUG
                    cout << "Unhnadled pipe: " << i << " event: " << pfd_irq[i].revents << endl;
#endif
                }

            }else{
//                usleep(2000);
            }

            if(mp.messagetosendlen>0)
            {

#ifdef SHOWDEBUG
                cout << ptsname(mp.fdm) <<": " << mp.messagetosendlen << endl;
#endif
                lock_guard<mutex> guard_SPI(M_spi);
                SET_CE(0);
                spi.transfer(mp.messagetosend,mp.messagetosendlen+1,mp.messagetosendlen+1);
                SET_RX(0);
                SET_CE(1);
//                usleep(15);

                pollc=POLL_RF;
                sendingon=&mp;

                break;
            }
        }
        if(delay>0) delay--;
    }
}

