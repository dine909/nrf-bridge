#include "configuration.h"

void Configuration::configureGPIO()
{
    fstream fs;
    const Json::Value gpio = root["gpio"];
    const string base = gpio["base"].asString();
    const Json::Value pins = gpio["pins"];
    for ( uint index = 0; index < pins.size(); ++index )
    {
        const Json::Value pin=pins[index];
        const string id=pin["id"].asString();
        cout<< "gpio:" << id<< endl;
        {
            fs.open(base+"/export", std::fstream::out );
            fs << id;
            fs.close();

            fs.open(base+"/gpio"+id+"/direction", std::fstream::out );
            fs << pin["dir"].asString();
            fs.close();
        }
        {
            std::string prefix = "IRQ";
            std::string argument = pin["name"].asString();
            if(argument.substr(0, prefix.size()) == prefix) {

                fs.open(base+"/gpio"+id+"/edge", std::fstream::out );
                fs << pin["edge"].asString();
                fs.close();

                string pn=base+"/gpio"+id;
                cout << "IRQ PIN: "<<pn << endl;
                irqPins.push_back(pn);
            }
        }
        {
            std::string prefix = "CE";
            std::string argument = pin["name"].asString();
            if(argument.substr(0, prefix.size()) == prefix) {

                string pn=base+"/gpio"+id;
                cout << "CE PIN: "<<pn << endl;
                cePins.push_back(pn);
            }
        }
    }
}

Configuration::Configuration()
{
    fstream fs;
    fs.open("config.json", std::fstream::in );
    fs >> root;
    fs.close();
    //string test;
    //fs >> test;
#ifdef ARM
    configureGPIO();
#endif
    //    root
}
