#include "radio.h"

Radio::Radio()
{

}

void Radio::setup(Configuration &conf){
    cout << "radio setup" << endl;
    Json::Value confpipes=conf.root["radio"]["pipes"];
    pipes.resize(confpipes.size());
    cout << "pipes: " << pipes.size() << endl;

    for ( uint index = 0; index < pipes.size(); ++index )
    {
        RadioPipe &rpipe=pipes[index];
        Json::Value jpipe=confpipes[index];
        rpipe.id=jpipe["id"].asUInt();
        string adr=jpipe["addr"].asString();
        memcpy(rpipe.address,adr.c_str(),adr.size());
        cout << "PIPE: " << rpipe.id << endl;
        rpipe.setup();
    }
}

void Radio::update()
{


}


