#include "mpty.h"
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <errno.h>
#include <termios.h>

MPTY::MPTY() : fdm(0),slave(0)
{

}

int setnonblock(int sock) {
   int flags;
   flags = fcntl(sock, F_GETFL, 0);
   if (-1 == flags)
      return -1;
   return fcntl(sock, F_SETFL, flags | O_NONBLOCK);
}

void MPTY::setRawMode(int f)
{
#ifdef ARM
    struct termios slave_orig_term_settings; // Saved terminal settings
    struct termios new_term_settings; // Current terminal settings

    rc = tcgetattr(f, &slave_orig_term_settings);

    // Set raw mode on the slave side of the PTY
    new_term_settings = slave_orig_term_settings;
    cfmakeraw (&new_term_settings);
    tcsetattr (f, TCSANOW, &new_term_settings);
#endif
}

int MPTY::setup()
{


    // Display /dev/pts
    //    system("ls -l /dev/pts");

    fdm = posix_openpt(O_RDWR );
    if (fdm < 0)
    {
        fprintf(stderr, "Error %d on posix_openpt()\n", errno);
        return 1;
    }

    rc = grantpt(fdm);
    if (rc != 0)
    {
        fprintf(stderr, "Error %d on grantpt()\n", errno);
        return 1;
    }

    rc = unlockpt(fdm);
    if (rc != 0)
    {
        fprintf(stderr, "Error %d on unlockpt()\n", errno);
        return 1;
    }

    // Display the changes in /dev/pts
    //    system("ls -l /dev/pts");

    printf("The slave side is named : %s on fd %d\n", ::ptsname(fdm),fdm);

    slave=open(::ptsname(fdm),O_RDWR); // keeps master open

    //    th=thread(MPTY::run,((void*)this));
    setnonblock(fdm);

    // Save the default parameters of the slave side of the PTY
    setRawMode(fdm);
    setRawMode(slave);
    return 0;
}

ssize_t MPTY::read(void *dest, size_t size){
    if(!fdm) return 0;
    return ::read(fdm,dest,size);
}

ssize_t MPTY::write(void *dest, size_t size){
    if(!fdm) return 0;
    ssize_t r=::write(fdm,dest,size);
//    ::tcdrain(fdm) ;
    return r;
}


void MPTY::close(){
    if(fdm){
        printf("Closing: %s\n", ::ptsname(fdm));
        ::close(fdm);
        fdm=0;
    }
}

MPTY::~MPTY(){
}

void MPTY::destroy(){
    close();
}
