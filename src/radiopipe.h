#ifndef RADIOPIPE_H
#define RADIOPIPE_H

#include "common.h"
#include "mpty.h"
#include <thread>

class RadioPipe :public MPTY
{
public:
    int id;
    char address[5];
    string messagetosend;
    int messagetosendlen;
    virtual void setup();

    RadioPipe();
};

#endif // RADIOPIPE_H
