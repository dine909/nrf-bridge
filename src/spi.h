#ifndef SPI_H
#define SPI_H

#include "common.h"

class SPI
{
protected:
public:
     int fd;
     string name;
     uint speed;
     uint delay;
     char datain[64],dataout[64];
     int transfer(int len);

//     template<typename T>
     void transfer(int len,const string data){
         memcpy(dataout,data.c_str(),data.length());
         transfer(len);
     }
     string transfer(const string data,int len=-1,int dlen=-1){
         string sret;
         memcpy(dataout,data.c_str(),dlen>0?dlen:data.length());
         int tlen=len>=0?len:data.length();
         transfer(tlen);
         char ret[tlen+1];
         memcpy(ret,datain,tlen);
         ret[tlen]=0;
         sret.append(ret,tlen);
         return sret;
//         string =char[data.length()]=
     }

     SPI();
     void dumpstat(const char *name);
     void setup(Configuration &conf);
     void setSpeed(uint32_t &hz);
};

#endif // SPI_H
