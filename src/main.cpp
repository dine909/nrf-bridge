#include "mainapp.h"
#include <stdio.h>
#include <unistd.h>
#include <signal.h>

volatile sig_atomic_t ctrlCcount = 0;

void catch_int(int sig_num){
  ctrlCcount++;
}


int main( int argc, char **argv){
    int res=chdir(argv[0]);
//    cout << argv[0] << endl;

    {
        MainApp app;
        app.setup();
        //     signal(SIGINT, catch_int);
        while(ctrlCcount==0)
        {
            app.update();
            usleep(500);
        }
        cout<<"  Exiting" << endl;
    }
    return 0;
}

