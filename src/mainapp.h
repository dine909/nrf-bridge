#ifndef MAINAPP_H
#define MAINAPP_H

#include "common.h"
#include "mpty.h"
#include "nrf24l01.h"
#include "configuration.h"

class MainApp
{
    NRF24L01 radio;
    Configuration config;
public:
    MainApp();
    void setup();
    void update();
    void destroy();

    ~MainApp();
};

#endif // MAINAPP_H
