#ifndef NRF24L01_H
#define NRF24L01_H

#include "spi.h"
#include "radio.h"
#include "nRF24L01-cmd.h"

class NRF24L01 : public Radio
{
    static SPI spi;
    vector<thread*> threads;
    static char REG_STATUS;
    static char REG_CONFIG;

    static ofstream *CEfs;
public:

    NRF24L01();
    void setup(Configuration &conf);
    virtual void update();
    static string transfer(string data,int len=-1);

    static void writeReg(string data);
    static char readReg(string data);
    static string readReg(string data,int registers);

    void irq(Configuration &conf);

//    static void pipeWorker(void* pipe);
    void setupCE(Configuration &conf, int id);
    void getStatus();
    void writeAddrReg(string radioAddr, char regAddr);


};

#endif // NRF24L01_H
