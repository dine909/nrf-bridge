#ifndef UTILS_H
#define UTILS_H

#include <ctime>
#include <iostream>
using namespace std;

class Utils
{
public:
    Utils();
    static const time_t getTimeMs();
    static void dumpHex(const void *data, size_t size);
};

#endif // UTILS_H
