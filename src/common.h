#ifndef COMMON_H
#define COMMON_H

#include "utils.h"
#include <unistd.h>
#include <vector>
#include <thread>         // std::thread
#include <string.h>         // std::thread
// fstream::open / fstream::close
#include <fstream>      // std::fstream
#include "configuration.h"
#include <stdint.h>

typedef unsigned int uint;


#endif // COMMON_H
