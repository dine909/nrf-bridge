#ifndef RADIO_H
#define RADIO_H

#include "radiopipe.h"

class Radio
{
protected:

public:
    vector<RadioPipe> pipes;
    Radio();
    virtual void setup(Configuration &conf);
    virtual void update();
};

#endif // RADIO_H
