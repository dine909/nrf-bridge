CC = $${CROSS_COMPILE}cpp
CPP = $${CROSS_COMPILE}g++
CFLAGS := -c -Wall -g -Os -std=c11 -MMD
CPPFLAGS := -c -Wall -g -Os -std=c++11 -MMD
LD = $(CPP)
LIBS = m
LDFLAGS = -pthread
OBJDIR = obj
SRCDIR = src
PLATFORMDIR = platform
TARGET = nrfb
INCLUDES =
DEFINES = _XOPEN_SOURCE=700

THIS=Makefile

FILES +=$(shell find $(SRCDIR) -type f)
INCLUDES+=$(shell find $(SRCDIR) -type d)

ifeq ($(OS),Windows_NT)
    PLATFORM_DEFINES += WIN32
    ifeq ($(PROCESSOR_ARCHITECTURE),AMD64)
	PLATFORM_DEFINES += AMD64
    endif
    ifeq ($(PROCESSOR_ARCHITECTURE),x86)
	PLATFORM_DEFINES += IA32
    endif
else
    UNAME_S ?= $(shell uname -s)
    UNAME_P ?= $(shell uname -m)
    ifeq ($(UNAME_S),Linux)
	PLATFORM_DEFINES += LINUX
    endif
    ifeq ($(UNAME_S),Darwin)
	PLATFORM_DEFINES += OSX
    endif
    ifeq ($(UNAME_P),x86_64)
	PLATFORM_DEFINES += AMD64
    endif
    ifneq ($(filter %86,$(UNAME_P)),)
	PLATFORM_DEFINES += IA32
    endif
    ifneq ($(filter arm%,$(UNAME_P)),)
	PLATFORM_DEFINES += ARM
	FILES += $(wildcard platform/arm/*)
	INCLUDES+=$(shell find platform/arm -type d)
    else
	FILES += $(wildcard platform/other/*)
	INCLUDES+=$(shell find platform/other -type d)
    endif
endif

DEFINES+=$(PLATFORM_DEFINES)

SRC=$(filter %.cpp %.c,$(FILES))
HEADERS=$(filter %.h,$(FILES))

OBJECTS = $(addprefix $(OBJDIR)/,$(addsuffix .o,$(basename $(SRC))))
DEPS := $(OBJECTS:.o=.d)

dir_guard=@mkdir -p $(@D)


all: $(TARGET)

$(info Binary:		$(abspath $(TARGET)))
$(info Platform:	$(PLATFORM_DEFINES))
$(info )

$(TARGET): $(filter %.o,$(OBJECTS) $(THIS))
	$(info Linking $(TARGET))
	@$(LD) -o $@ $^ $(LDFLAGS) $(addprefix -l,$(LIBS))
	$(info Build completed successfully!)

-include $(DEPS)

$(OBJDIR)/%.o: %.c $(THIS)
	$(dir_guard)
	$(info Building $<)
	@$(CC) $(CFLAGS) $(addprefix -D,$(DEFINES)) $(addprefix -I,$(INCLUDES)) -o $@ $<

$(OBJDIR)/%.o: %.cpp $(THIS)
	$(dir_guard)
	$(info Building $<)
	@$(CPP) $(CPPFLAGS) $(addprefix -D,$(DEFINES)) $(addprefix -I,$(INCLUDES)) -o $@ $<

clean:
	$(info Cleaning)
	@rm -rf $(TARGET) $(OBJECTS) $(OBJDIR)



ifeq ($(filter add run ,$(MAKECMDGOALS)),$(firstword $(MAKECMDGOALS)))
  # use the rest as arguments for "run"
  RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  # ...and turn them into do-nothing targets
  $(eval $(RUN_ARGS):;@:)
endif

-include Makefile.*

run: $(TARGET)
	$(info Running $(TARGET))
	./$< $(RUN_ARGS)


.PHONY: print gitinit commit update pull push run add status



GIT=$(shell which git)
ORIGIN=master

#UPDATECHECK=$(shell (git diff | grep '^' >> /dev/null) || echo no-changed-files)
LASTLOG=$(shell git log --oneline -n1 | cut -d' ' -f2-)

.SECONDEXPANSION:
CM=$(shell read -p "Commit message [$(LASTLOG)]: " msg;msg=$${msg:-$(LASTLOG)};echo $${msg})

gitinit: $(GIT)
	$< init

update: $(GIT) $(UPDATECHECK)
	@$< diff --summary
	@$< add -u
	@$< status
	@$< commit -m"$(CM)"

commit: $(GIT) $(UPDATECHECK)
	@$< $@ -m"$(CM)"

push: $(GIT)
	@$< $@ origin $(ORIGIN)

pull: $(GIT)
	@$< $@ origin $(ORIGIN)

add: $(GIT) $(RUN_ARGS)
	@$< $@ $(RUN_ARGS)

status: $(GIT)
	@$< $@

.gitignore: $(GIT)
	@echo $(TARGET) > $@
	@echo $(OBJDIR)/\* >> $@

print:
	@echo $(PV)
